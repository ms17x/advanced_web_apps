# LIS4368 Advanced Web Apps Development

## Mitchnikov Seide

### LIS4368 Requirements:

*Course Work Links:*

[A1 README.md](a1/README.md "My A1 README.md file")

- Install JDK
- Install AMPPS
- provide screnshots of installations
- create Bitbucket repo
- complete Bitbucket tutorials
- provide git command descriptions

[A2 README.md](a2/README.md "My A2 README.md file")

- Download MySQL
- Deploy Web Servlets

[A3 README.md](a3/README.md "My A3 README.md file")

- Have MySQL running on a local connection
- Create an Entity Relationship Diagram (ERD)
- Include data

[A4 README.md](a4/README.md "My A4 README.md file")

- Basic Server-Side Validation
- JSP Forms

[P1 README.md](p1/README.md "My P1 README.md file")

- Desing a Web Application to Validate Data
- Review subdirectories and files, especially META-INF and WEB-INF
- Each assignment *must* have its own globalsubdirectory.

[A5 README.md](a5/README.md "My A5 README.md file")

- Server-Side Validation
- Insert Data into Local DB

[P2 README.md](p2/README.md "My P2 README.md file")

- Design a Web Application that uses CRUD functionality
- Be able to Correctly Dislpay List of Data


