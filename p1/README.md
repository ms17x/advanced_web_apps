# LIS4368 Advanced Webb Apps Development

## Mitchnikov Seide


### Project #1 Requirements:
Deliverables:

1. Design a web application that validates data with regular expressions

### README.md file should include the following items:
- Screenshot of P1 Failed Validation
- Screenshot of P1 Successful Validation

#### Assignment Screenshots:

![project 1 code and output](img/p1.PNG)
![project 1 code and output](img/2.PNG)
![project 1 code and output](img/3.PNG)


#### Tutorial Links:

http://localhost:9999/lis4368/

*Bitbucket Tutorial - Station Locations:*
[p1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

