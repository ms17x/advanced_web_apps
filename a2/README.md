# LIS4368 Advanced Web Applications Development

## Mitchnikov Seide


### Assignment #2 Requirements:
Three Parts:

- Download MySQL
- Deploy Web Servlets
- Questions from Chapters 5 and 6

### README.md file should include the following items:
- Assessment links

#### Assignment Screenshots:

*Screenshot code and output*:

http://localhost:9999/hello/
![assignment 1 isntall proof](img/1.PNG)

http://localhost:9999/hello/sayhello
![assignment 1 code and output](img/2.PNG)

http://localhost:9999/hello/querybook.html
![assignment 1 code and output](img/3.PNG)

![assignment 1 code and output](img/4.PNG)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*

[A Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A2 My Team Quotes Tutorial Link](https://bitbucket.org/ms17x/myteamquotes/ "My Team Quotes Tutorial")


