# LIS4368 Advanced Web Applications Development

## Mitchnikov Seide


### Assignment #4 Requirements:
Deliverables:

- Show Server-Side Validation
- Work with JSP Forms
- Provide Bitbucket read-only acces to ms17x repo

README.md file should include the following items:

- Screenshot of A4 Correct Validation
- Screenshot of A4 Failed Validation


#### Assignment Screenshots:

![assignment 4 code and output](img/a4.PNG)

![assignment 4 isntall proof](img/4.PNG)


#### Tutorial Links:

http://localhost:9999/lis4368/index.jsp

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ms17x/myteamquotes/ "My Team Quotes Tutorial")


