# LIS4368 Advanced Web Applications Development

## Mitchnikov Seide


### Assignment #3 Requirements:
Deliverables:

- Entity Relationship Diagram (ERD)
- Include at least 10 records in each table
- Provide Bitbucket read-only acces to cdy15 repo and inlude links to all of the following files:
- docs folder: a3.mwb and a3.sql
- img folder: a3.png
- README.md
- Links: Bitbucket Repo

#### Assignment Screenshots:

![assignment 3 isntall proof](img/a3.PNG)

*A3 docs: a3.mwb and a3.sql*
[A3 MWB File](docs/A3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/A3.sql "A3 SQL Script")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*

[A Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
https://bitbucket.org/ms17x/lis4368/src/master/

*Tutorial: Request to update a teammate's repository:*
[A2 My Team Quotes Tutorial Link](https://bitbucket.org/ms17x/myteamquotes/ "My Team Quotes Tutorial")
