# LIS4368 Advanced Web Applications Development

## Mitchnikov Seide


### Assignment p2 Requirements:
Deliverables:

- Desing a web application that uses CRUD functionality
- Be able to display list of customers in application

README.md file should include the following items
- Screenshot of valid user form
- Screenshot of passed validation
- Screenshot of displayed data
- Screenshot of modified data
- Screenshot of delete warning
- Screenshot of associated changes

#### Assignment Screenshots:

![assignment 1 code and output](img/1.PNG)

![assignment 1 isntall proof](img/2.PNG)

![assignment 1 code and output](img/3.PNG)

![assignment 1 code and output](img/4.PNG)

![assignment 1 code and output](img/5.PNG)

![assignment 1 code and output](img/6.PNG)

![assignment 1 code and output](img/7.PNG)



#### Tutorial Links:

http://localhost:9999/lis4368/

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ms17x/myteamquotes/ "My Team Quotes Tutorial")


