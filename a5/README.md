# LIS4368 Advanced Web Applications Development

## Mitchnikov Seide


### Assignment #5 Requirements:
Deliverables:

- Show Server-Side Validation
- Provide Screenshot to inserted data
- Provide Bitbucket read-only acces to ms17x repo

README.md file should include the following items

- Screenshot of Valid User Form Entry
- Screenshot of Passed Validation
- Screenshot of Associated Database Entry

#### Assignment Screenshots:

![assignment 1 code and output](img/1.5.PNG)

![assignment 1 isntall proof](img/2.5.PNG)

![assignment 1 code and output](img/3.5.PNG)


#### Tutorial Links:

http://localhost:9999/lis4368/

